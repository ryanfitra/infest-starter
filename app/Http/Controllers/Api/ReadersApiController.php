<?php

namespace App\Http\Controllers\Api;

use App\Models\Readers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReadersApiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Readers $readers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Readers $readers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Readers $readers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Readers $readers)
    {
        //
    }
}
