import lodash from 'lodash';
window._ = lodash;

import { serialize } from 'object-to-formdata'
window.serialize = serialize

import axios from 'axios';
window.axios = axios;

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.axios.defaults.baseURL = '/api/'
window.axios.interceptors.response.use(
    function(response) {
        return response
    },
    function(error) {
        if (error.response.status === 401) {
            location.assign('/login')
        }

        return Promise.reject(error)
    }
)