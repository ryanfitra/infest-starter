import './bootstrap';

import Vue from 'vue'

Vue.config.productionTip = false
    // Vue.prototype.$jquery = $

import App from './App.vue'

// Router
import router from './router'

// Plugins
import GlobalComponents from './globalComponents'

Vue.use(GlobalComponents)

new Vue({
    render: h => h(App),
    router
}).$mount("#app")