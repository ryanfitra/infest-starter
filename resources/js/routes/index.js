import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter)

const View = { template: '<router-view></router-view>'}

import Frame from "@views/layout/Frame.vue"
import Dashboard from "@views/Dashboard.vue"
import Books from "@views/books/Index.vue"
import Readers from "@views/readers/Index.vue"


const routes = [
  {
      path: "/",
      name: "Home",
      component: Frame,
      redirect: 'dashboard',
      children: [
          {
              path: "dashboard",
              name: "dashboard",
              component: Dashboard
          },
          {
              path: "books",
              name: "books",
              component: Books
          },
          {
              path: "readers",
              name: "readers",
              component: Readers
          }
      ]
  }]

export default new VueRouter({
    mode: 'history',
    base: '/',
    routes
})
