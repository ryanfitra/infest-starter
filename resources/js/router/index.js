import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const View = { template: '<router-view></router-view>' }

import Login from "@views/Login.vue"
import Register from "@views/Register.vue"

import Frame from "@views/layout/Frame.vue"
import Dashboard from "@views/Dashboard.vue"
import Books from "@views/books/Index.vue"
import Readers from "@views/readers/Index.vue"

const routes = [
    {
        path: '/',
        component: Frame,
        redirect: 'dashboard',
        children: [{
            path: 'dashboard',
            name: 'dashboard',
            component: Dashboard,
        },
            {
                path: 'books',
                name: 'books',
                component: Books,
            },
            {
                path: 'readers',
                name: 'readers',
                component: Readers,
            },
        ]
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
    },
    {
        path: '/register',
        name: 'register',
        component: Register,
    }
]

export default new VueRouter({
    mode: 'history',
    base: '/',
    routes
})
