<?php

namespace Database\Seeders;

use App\Models\Readers;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ReadersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Readers::factory()->count(5)->create();
    }
}
