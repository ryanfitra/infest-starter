<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Books>
 */
class BooksFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => fake()->sentence(2),
            'category' => fake()->randomElement(['novel', 'dictionary', 'comic', 'biography']),
            'author' => fake()->name(),
            'publisher' => fake()->name(),
            'page' => fake()->numberBetween(30, 999),
            'price' => fake()->numberBetween(3, 299).'000',
            'desc' => fake()->paragraph(5),
        ];
    }
}
