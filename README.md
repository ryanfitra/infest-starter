Infest Test

## Install

**Database**
- Buat database baru dengan nama `infest`

**Laravel**
- Copy `.env.example` file ke `.env`, nama database sudah dibuat dengan nama `infest`
- Run `composer install`
- Run `php artisan key:generate`
- Run `php artisan migrate --seed` (sudah otomatis menjalankan data sampel)

**Vue.js**
- Run `npm install` (install dependencies dari `package.json`)
- Run `npm run dev` (untuk memulai development)
- Run `npm run build` (build aplikasi)

## Command Usage For Laravel

Membuat tabel dari database migration baru
```
php artisan make:migration create_TABLENAME_table
```
Membuat model
```
php artisan make:mode NamaModel
```
Membuat model beserta dengan migration (`m`), factory (`f`), seeder (`s`) dan controller (`c`)
```
php artisan make:mode NamaModel -mfsc
```
Membuat controller
```
php artisan make:controller NamaController
```
Membuat controller beserta dengan struktur resource
```
php artisan make:controller NamaController --resource
```
Membuat controller beserta dengan struktur resource dan menambahkan model yang telah dibuat
```
php artisan make:controller NamaController --resource --model=NamaModel
```
Membuat seeder
```
php artisan make:seeder NamaSeeder
```

--**Menjalankan file pada laravel**--

Menjalankan tabel yang dibuat dengan spesifik file
```
php artisan make:refresh --path=/database/migrations/MIGRATE_TABLE_NAME.php
```
Menjalankan data seeder untuk tabel yang dibuat dengan spesifik file
```
php artisan db:seed --class=NamaSeeder 
```

## Add Package For Vue.js

For example
```
npm install vue-sweetaler2
npm i bootstrap@4
npm install @casl/vue@1.x @casl/ability
```